﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class WorkerBuyingCoffee : StateBehaviour
{
    public GameObject coffeeBuypoint;
    public CoffeeStand coffeeStandScript;
    public BoolVar hasCoffee;

    void Awake()
    {
        hasCoffee = blackboard.GetBoolVar("hasCoffee");
    }
    // Called when the state is enabled
    void OnEnable () {
		//Debug.Log(gameObject.ToString() + "Started Buying Coffee");
	}
 
	// Called when the state is disabled
	void OnDisable () {
		//Debug.Log(gameObject.ToString() + "Stopped Buying Coffee");
	}
	
	// Update is called once per frame
	void Update () {
        if (coffeeStandScript.GetNumberOfCoffees() < 1)
        {
            blackboard.SendEvent("coffeeGone");
        }
        if (this.transform.position != coffeeBuypoint.transform.position)
        {
            //Walk towards the line
            transform.position = Vector3.MoveTowards(this.transform.position, coffeeBuypoint.transform.position, 1.5f * Time.deltaTime);
        }
        else
        {
            coffeeStandScript.RemoveCoffee();
            hasCoffee.Value = true;
            blackboard.SendEvent("boughtCoffee");
        }
    }
}


