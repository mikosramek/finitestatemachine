﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class WorkingDrinkingCoffee : StateBehaviour
{
    BoolVar hasCoffee;
    FloatVar thirst;
    public float thirstDepletionRate;
    public float firstThirstValue;
    public GameObject drinkingMan;

    void Awake()
    {
        hasCoffee = blackboard.GetBoolVar("hasCoffee");
        thirst = blackboard.GetFloatVar("Thirst");
    }

    // Called when the state is enabled
    void OnEnable () {
		//Debug.Log(gameObject.ToString() + "Started Drinking Coffee");
        firstThirstValue = thirst.Value;
        drinkingMan.SetActive(true);
	}
 
	// Called when the state is disabled
	void OnDisable () {
        //Debug.Log(gameObject.ToString() + "Stopped Drinking Coffee");
        drinkingMan.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        DrinkCoffee();
	}


    private void DrinkCoffee()
    {
        thirst.Value-=thirstDepletionRate;
        if(thirst.Value <= firstThirstValue - 20)
        {
            blackboard.SendEvent("tiredOfCoffee");
        }
        if(thirst.Value <= 0)
        {
            blackboard.SendEvent("doneCoffee");
            hasCoffee.Value = false;
        }
    }
}


