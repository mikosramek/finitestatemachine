﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class WorkerWandering : StateBehaviour
{
    FloatVar energy, thirst, maxEnergy;
    public GameObject[] waypoints;
    public GameObject currentDestination;
    public float energyRepletionRate;
    public float thirstRate;
    public float wanderSpeed;
    private int waypointIndex;
    private BoolVar hasCoffee;
    public GameObject walkingMan;


    void Awake()
    {
        energy = blackboard.GetFloatVar("Energy");
        thirst = blackboard.GetFloatVar("Thirst");
        maxEnergy = blackboard.GetFloatVar("MaxEnergy");
        hasCoffee = blackboard.GetBoolVar("hasCoffee");
    }

    void OnEnable () {
		//Debug.Log(gameObject.ToString() + "Started Wandering");
        waypoints = new GameObject[GameObject.FindGameObjectsWithTag("workerWaypoint").Length];
        waypoints = GameObject.FindGameObjectsWithTag("workerWaypoint");
        currentDestination = waypoints[0];
        waypointIndex = 0;
        walkingMan.SetActive(true);
    }
 
	void OnDisable () {
        //Debug.Log(gameObject.ToString() + "Stopped Wandering");
        walkingMan.SetActive(false);

    }
	
	void Update () {
        Wander();
        if (energy.Value < 101)
        {
            energy.Value += energyRepletionRate;
        }
        if (thirst.Value < 101)
        {
            thirst.Value += thirstRate;
        }
        if (energy.Value >= 100 && !hasCoffee.Value)
        {
            Rested();
        }       
        if (thirst.Value >= 100 && !hasCoffee.Value)
        {
            Thirsty();
        }
        


    }


    private void ChooseNewDestination()
    {
        //print("Finding new destination");
        int tempIndex = waypointIndex;
        while(currentDestination == waypoints[tempIndex])
        {
            waypointIndex = (int)Random.Range(0, waypoints.Length);
            currentDestination = waypoints[waypointIndex];
        }
        if (hasCoffee.Value)
        {
            DrinkCoffee();
        }
    }

    private void Wander()
    {
        FlipCharacter();
        transform.position = Vector3.MoveTowards(transform.position, currentDestination.transform.position, 1f * Time.deltaTime * wanderSpeed);
        if(transform.position == currentDestination.transform.position)
        {
            ChooseNewDestination();
            
        }
    }
    

    private void Rested()
    {
        blackboard.SendEvent("rested");
    }
    private void Thirsty()
    {
        blackboard.SendEvent("thirsty");
    }
    private void DrinkCoffee()
    {
        blackboard.SendEvent("realisedTheyHaveCoffee");
    }


    private void FlipCharacter()
    {
        if((currentDestination.transform.position - this.transform.position).x > 0)
        {
            this.transform.localScale= new Vector3(1, 1, 1);
        }else
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }

    }

}


