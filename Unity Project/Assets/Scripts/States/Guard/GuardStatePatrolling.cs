﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class GuardStatePatrolling : StateBehaviour
{

    private int waypointIndex;
    public Transform[] waypoints;
    public float patrolSpeed;
    private SpriteRenderer rend;
    public Sprite patrolling;
    GameObjectVar spriteMan;

    GameObjectVar target;
    //If using a "float"
    //have to use variableName.Value

    void Awake()
    {
        spriteMan = blackboard.GetGameObjectVar("spriteMan");
        rend = spriteMan.Value.GetComponent<SpriteRenderer>();
        target = blackboard.GetGameObjectVar("target");
    }


    // Called when the state is enabled
    void OnEnable () {
		Debug.Log(gameObject.ToString() + "Started Patrolling");
        rend.sprite = patrolling;
    }
 
	// Called when the state is disabled
	void OnDisable () {
		Debug.Log(gameObject.ToString() + "Stopped Patrolling");
	}
	
	// Update is called once per frame
	void Update () {
        MoveToWaypoint();
        FlipCharacter();

       
    }

    private void MoveToWaypoint()
    {
        if (transform.position != waypoints[waypointIndex].position)
        {
            transform.position = Vector3.MoveTowards(this.transform.position, waypoints[waypointIndex].position, patrolSpeed * Time.deltaTime);
        }
        else
        {
            waypointIndex++;
            if (waypointIndex == waypoints.Length)
            {
                waypointIndex = 0;
            }
        }
    }

    public void EnemyEnterVision(Collider col)
    {
        //Warn
        target.Value = col.gameObject;
        blackboard.SendEvent("SeenPlayer");
    }
    public void FriendlyEnterVision(Collider col)
    {
        //Salute
        blackboard.SendEvent("WorkerInRange");
    }
    private void FlipCharacter()
    {
        if ((waypoints[waypointIndex].transform.position - this.transform.position).x > 0)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            this.transform.localScale = new Vector3(1, 1, 1);
        }

    }
}


