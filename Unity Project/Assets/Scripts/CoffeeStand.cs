﻿using UnityEngine;
using System.Collections;

public class CoffeeStand : MonoBehaviour {

    public int numberOfCoffees;
    public int maxNumberOfCoffees;

    public GameObject[] coffees;

	void Start () {
        numberOfCoffees = 0;
        
        UpdateVisuals();

    }
	
    private void UpdateVisuals()
    {
        for (int index = 0; index < maxNumberOfCoffees; index++)
        {
            coffees[index].SetActive(false);
        }
        for (int index = 0; index < numberOfCoffees; index++)
        {
            coffees[index].SetActive(true);
        }
    }

    public int GetNumberOfCoffees()
    {
        return numberOfCoffees;
    }

    public void RemoveCoffee()
    {
        numberOfCoffees--;
        UpdateVisuals();
    }

    public void AddCoffee()
    {
        numberOfCoffees++;
        UpdateVisuals();
    }

    public bool IsCoffee()
    {
        if(numberOfCoffees > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
	
}
