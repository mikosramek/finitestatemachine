﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour {

    public bool active;
    private float foodValue;
    public GameObject foodSprite;

    void Awake()
    {
        UpdateGameObject();
        StartCoroutine(Eaten());
        
    }

    public float GetFoodValue()
    {
        return foodValue;
    }

    public void SetEaten()
    {
        active = false;
        UpdateGameObject();
        StartCoroutine(Eaten());
    }

    public bool IsActive()
    {
        return active;
    }

    void UpdateGameObject()
    {
        if (active)
        {
            foodSprite.SetActive(true);
        }
        else
        {
            foodSprite.SetActive(false);
        }
        transform.localScale = new Vector3(foodValue, foodValue, foodValue);
    }


    private IEnumerator Eaten()
    {
        yield return new WaitForSeconds(Random.Range(20, 40));
        foodValue = Random.Range(0.2f, 0.7f);
        active = true;
        UpdateGameObject();
        yield return null;
    }
}
