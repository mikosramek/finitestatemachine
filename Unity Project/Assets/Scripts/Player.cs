﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    public float speed;
    public float health;
    public GameObject runningMan, stillMan;
    public SpriteRenderer runSprite, stillSprite;
    Color healthColor;

    void Start()
    {
        healthColor = new Color(1, 1, 1);
    }

	void Update () {
        if (Input.GetKey(KeyCode.A))
        {
            this.transform.position -= Vector3.right * Time.deltaTime * speed;
            this.transform.localScale = new Vector3(-1, 2, 1);
            runningMan.SetActive(true);
            stillMan.SetActive(false);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.transform.position += Vector3.right * Time.deltaTime * speed;
            this.transform.localScale = new Vector3(1, 2, 1);
            runningMan.SetActive(true);
            stillMan.SetActive(false);
        }
        else
        {
            runningMan.SetActive(false);
            stillMan.SetActive(true);
        }


        healthColor = new Color(1, 0 + health / 100, 0 + health / 100);

        stillSprite.color = healthColor;
        runSprite.color = healthColor;

        if(health <= 0)
        {
            Reset();
        }

	}


    public void GetShot(float damage)
    {
        health -= damage;
    }

    private void Reset()
    {
        health = 100;
        transform.position = new Vector3(-5.81f, 1, -0.75f);
    }
}
