﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class CoffeeMakerIdle : StateBehaviour
{
    public CoffeeStand cofStand;
    public float checkRate;
    private float checkTime;
    public GameObject idle;
	// Called when the state is enabled
	void OnEnable () {
        checkTime = 0;
        idle.SetActive(true);
		//Debug.Log(gameObject.ToString() + "Started Idle");
	}
 
	// Called when the state is disabled
	void OnDisable () {
        idle.SetActive(false);
        //Debug.Log(gameObject.ToString() + "Stopped Idle");
    }
	
	// Update is called once per frame
	void Update () {
        checkTime += checkRate;
	    if(cofStand.GetNumberOfCoffees() < cofStand.maxNumberOfCoffees && checkTime > 1)
        {
            blackboard.SendEvent("coffeesGone");
        }
	}
}


