﻿using UnityEngine;
using System.Collections;

public class Vision : MonoBehaviour {
    public GameObject visionary;
    public string targetTag, friendlyTag;
    

    public void OnTriggerEnter(Collider other)
    {
        print("Entered Vision");
        if(other.gameObject != null)
        {
            if(other.gameObject.tag == targetTag)
            {
                visionary.SendMessage("EnemyEnterVision", other);
            }else if(other.gameObject.tag == friendlyTag)
            {
                visionary.SendMessage("FriendlyEnterVision", other);
            }
        }
    }
   

    public void OnTriggerExit(Collider other)
    {
        print("Left Vision");
        if (other.gameObject != null)
        {
            if (other.gameObject.tag == targetTag)
            {
                visionary.SendMessage("EnemyExitVision", other);
            }
            else if (other.gameObject.tag == friendlyTag)
            {
                visionary.BroadcastMessage("FriendlyExitVision", other);
            }
        }
    }
}
