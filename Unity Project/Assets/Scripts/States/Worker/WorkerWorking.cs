﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class WorkerWorking : StateBehaviour
{
    FloatVar energy, thirst, maxEnergy;
    public GameObject[] workStations;
    public GameObject currentWorkStation;
    public float energyDepletionRate;
    public float thirstRate;
    public GameObject sittingMan, walkingMan;

    void Awake()
    {
        energy = blackboard.GetFloatVar("Energy");
        thirst = blackboard.GetFloatVar("Thirst");
        maxEnergy = blackboard.GetFloatVar("MaxEnergy");
    }
    void OnEnable () {
        walkingMan.SetActive(true);
        //Debug.Log(gameObject.ToString() + "Started Working");
        workStations = new GameObject[GameObject.FindGameObjectsWithTag("workerWorkstation").Length];
        workStations = GameObject.FindGameObjectsWithTag("workerWorkstation");
        for (int i = 0; i < workStations.Length; i++)
        {
            if (workStations[i].GetComponent<WorkStation>().IsOpen())
            {
                currentWorkStation = workStations[i];
                currentWorkStation.GetComponent<WorkStation>().CloseWorkStation();
                return;
            }
        }
    }
 
	void OnDisable () {
        //Debug.Log(gameObject.ToString() + "Stopped Working");
        sittingMan.SetActive(false);
    }
	
	void Update () {
        if(currentWorkStation != null)
        {
            if(this.transform.position != currentWorkStation.transform.position)
            {
                FlipCharacter();
                transform.position = Vector3.MoveTowards(this.transform.position, currentWorkStation.transform.position, 1f * Time.deltaTime);               
            }
            else
            {
                walkingMan.SetActive(false);
                sittingMan.SetActive(true);
                Work();
            }
        }
        
	}

    private void Work()
    {
        //transform.position = currentWorkStation.transform.position;
        energy.Value -= energyDepletionRate;
        thirst.Value += thirstRate;

        if(energy.Value <= 0)
        {
            blackboard.SendEvent("tired");
            currentWorkStation.GetComponent<WorkStation>().OpenWorkStation();
            currentWorkStation = null;
        }
        if(thirst.Value >= 100)
        {
            blackboard.SendEvent("thirsty");
            currentWorkStation.GetComponent<WorkStation>().OpenWorkStation();
            currentWorkStation = null;
        }

    }
    private void FlipCharacter()
    {
        if ((currentWorkStation.transform.position - this.transform.position).x > 0)
        {
            this.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }

    }
}


