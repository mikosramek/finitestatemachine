﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class GuardStateShooting : StateBehaviour
{
    GameObjectVar target;
    FloatVar shootingRange;
    private SpriteRenderer rend;
    public float gunDamage;
    public Sprite shooting;
    GameObjectVar spriteMan;

    void Awake()
    {
        spriteMan = blackboard.GetGameObjectVar("spriteMan");

        rend = spriteMan.Value.GetComponent<SpriteRenderer>();
        target = blackboard.GetGameObjectVar("target");
        shootingRange = blackboard.GetFloatVar("shootingRange");
    }


    // Called when the state is enabled
    void OnEnable () {
		Debug.Log(gameObject.ToString() + "Started Shooting");
        rend.sprite = shooting;
    }
 
	// Called when the state is disabled
	void OnDisable () {
		Debug.Log(gameObject.ToString() + "Stopped Shooting");
	}
	
	// Update is called once per frame
	void Update () {
        WatchPlayer();
        ShootPlayer();
        FlipCharacter();
	}

    private void WatchPlayer()
    {
        
        if (target != null && (target.transform.position - this.transform.position).magnitude > shootingRange)
        {
            SendEvent("PlayerLeftRange");
        }

    }
    private void ShootPlayer()
    {
        Player tempPlayer = target.Value.GetComponent<Player>();
        tempPlayer.GetShot(gunDamage);
    }
    private void FlipCharacter()
    {
        if ((target.transform.position - this.transform.position).x > 0)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            this.transform.localScale = new Vector3(1, 1, 1);
        }

    }
}


