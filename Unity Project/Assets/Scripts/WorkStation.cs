﻿using UnityEngine;
using System.Collections;

public class WorkStation : MonoBehaviour {
    public bool open;

    void Start()
    {
        open = true;
    }

    public bool IsOpen()
    {
        return open;
    }

    public void OpenWorkStation()
    {
        open = true;
    }
    public void CloseWorkStation()
    {
        open = false;
    }
}
