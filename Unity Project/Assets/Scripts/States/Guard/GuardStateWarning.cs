﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class GuardStateWarning : StateBehaviour
{
    GameObjectVar target;
    FloatVar shootingRange;
    private SpriteRenderer rend;
    public Sprite warning;
    GameObjectVar spriteMan;


    void Awake()
    {
        spriteMan = blackboard.GetGameObjectVar("spriteMan");

        rend = spriteMan.Value.GetComponent<SpriteRenderer>();
        target = blackboard.GetGameObjectVar("target");
        shootingRange = blackboard.GetFloatVar("shootingRange");
    }

   

    // Called when the state is enabled
    void OnEnable () {
		Debug.Log(gameObject.ToString() + "Started Warning");
        rend.sprite = warning;

    }
 
	// Called when the state is disabled
	void OnDisable () {
		Debug.Log(gameObject.ToString() + "Stopped Warning");
	}
	
	// Update is called once per frame
	void Update () {
        WatchPlayer();
        FlipCharacter();
    }

    private void WatchPlayer()
    {
        if (target.Value != null && (target.Value.transform.position - this.gameObject.transform.position).magnitude < shootingRange)
        {
            blackboard.SendEvent("PlayerTooClose");
        }
    }

    public void EnemyExitVision(Collider col)
    {
        target.Value = null;
        blackboard.SendEvent("PlayerLeft");
    }
    public void FriendlyEnterVision(Collider col)
    {
        //Salute
        blackboard.SendEvent("WorkerInRange");
    }

    private void FlipCharacter()
    {
        if ((target.transform.position - this.transform.position).x > 0)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            this.transform.localScale = new Vector3(1, 1, 1);
        }

    }
}


