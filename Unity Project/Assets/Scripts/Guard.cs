﻿using UnityEngine;
using System.Collections;

public class Guard : MonoBehaviour {

    public enum GuardState
    {
        patrolling,
        saluting,
        warning,
        shooting
    }

    public GuardState state;
    public GameObject target;
    public float shootingRange;
    private Renderer rend;
    private int waypointIndex;
    public Transform[] waypoints;
    public float patrolSpeed;
	void Start () {
        state = GuardState.patrolling;
        rend = GetComponent<Renderer>();
        waypointIndex = 0;
	}
	
	void Update () {
        switch (state)
        {
            case GuardState.patrolling:
                Patrolling();
                break;
            case GuardState.saluting:
                Saluting();
                break;
            case GuardState.warning:
                Warning();
                break;
            case GuardState.shooting:
                Shooting();
                break;
            default:
                Patrolling();
                break;
        }
	}

    public void EnemyEnterVision(Collider col)
    {
        //Warn
        target = col.gameObject;
        state = GuardState.warning;
    }
    public void FriendlyEnterVision(Collider col)
    {
        //Salute
        state = GuardState.saluting;
    }
    public void EnemyExitVision(Collider col)
    {
        //Warn
        target = null;
        state = GuardState.patrolling;      
    }
    public void FriendlyExitVision(Collider col)
    {
        //Salute
        state = GuardState.patrolling;
        if(target != null)
        {
            state = GuardState.warning;
        }
    }
    private void Patrolling()
    {
        rend.material.color = Color.white;

        if (transform.position != waypoints[waypointIndex].position)
        {
            transform.position = Vector3.MoveTowards(this.transform.position, waypoints[waypointIndex].position, patrolSpeed * Time.deltaTime);
        }else
        {
            waypointIndex++;
            if(waypointIndex == waypoints.Length)
            {
                waypointIndex = 0;
            }
        }

    }
    private void Saluting()
    {
        rend.material.color = Color.green;
    }

    private void Warning()
    {
        rend.material.color = Color.yellow;
        if (target != null && (target.transform.position - this.transform.position).magnitude < shootingRange)
        {
            state = GuardState.shooting;
        }
    }

    private void Shooting()
    {
        rend.material.color = Color.red;
        if (target != null && (target.transform.position - this.transform.position).magnitude > shootingRange)
        {
            state = GuardState.warning;
        }
    }

}
