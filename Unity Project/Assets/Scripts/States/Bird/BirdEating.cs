﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class BirdEating : StateBehaviour
{

    FloatVar movementSpeed;
    FloatVar hunger;
    GameObjectVar foodTarget;

    public Sprite flying, eating;
    public SpriteRenderer spriteR;

    void Awake()
    {
        hunger = blackboard.GetFloatVar("hunger");
        movementSpeed = blackboard.GetFloatVar("movementSpeed");
        foodTarget = blackboard.GetGameObjectVar("foodTarget");
    }


    // Called when the state is enabled
    void OnEnable () {
		//Debug.Log("Started Eating");
	}
 
	// Called when the state is disabled
	void OnDisable () {
		//Debug.Log("Stopped Eating");
	}
	
	// Update is called once per frame
	void Update () {
	    if(transform.position != foodTarget.Value.transform.position)
        {
            transform.position = Vector3.MoveTowards(this.transform.position, foodTarget.Value.transform.position, movementSpeed.Value * Time.deltaTime);
            transform.LookAt(foodTarget.Value.transform);
        }
        else
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
            spriteR.sprite = eating;
            StartCoroutine(EatFood());
        }
	}

    private IEnumerator EatFood()
    {
        Food tempFood = foodTarget.Value.GetComponent<Food>();
        float foodValue = tempFood.GetFoodValue();

        for(int i = 0; i < 100; i++)
        {
            if (hunger > 0)
            {
                hunger.Value -= foodValue / 100;
            }
            yield return new WaitForSeconds(0.01f);
        }

        tempFood.SetEaten();
        if(hunger.Value <= 0)
        {
            blackboard.SendEvent("full");
            StopAllCoroutines();
        }
        else
        {        
            blackboard.SendEvent("hungry");
            StopAllCoroutines();
        }
        yield return null;
    }
}


