﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class CoffeeMakerBrewing : StateBehaviour
{
    public CoffeeStand cofStand;
    public float brewingRate;
    
    private float brewedAmount;
    public GameObject brewing;
    // Called when the state is enabled
    void OnEnable () {
		//Debug.Log(gameObject.ToString() + "Started Brewing");
        brewedAmount = 0;
        brewing.SetActive(true);
	}
 
	// Called when the state is disabled
	void OnDisable () {
        brewing.SetActive(false);
        //Debug.Log(gameObject.ToString() + "Stopped Brewing");
    }
	
	// Update is called once per frame
	void Update () {

        brewedAmount += brewingRate;

        if(brewedAmount >= 1)
        {
            cofStand.AddCoffee();
            blackboard.SendEvent("coffeesFull");
        }
        

    }
}


