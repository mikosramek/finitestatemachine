﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class WorkerWaitingInLine : StateBehaviour
{

    public GameObject coffeeLineWaypoint;
    public CoffeeStand coffeeStandScript;
    public GameObject walkingMan;
	// Called when the state is enabled
	void OnEnable () {
        //Debug.Log(gameObject.ToString() + "Started Waiting For Coffee");
        walkingMan.SetActive(true);
	}
 
	// Called when the state is disabled
	void OnDisable () {
        //Debug.Log(gameObject.ToString() + "Stopped Waiting For Coffee");
        //walkingMan.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	    if(this.transform.position != coffeeLineWaypoint.transform.position)
        {
            //Walk towards the line
            FlipCharacter();
            transform.position = Vector3.MoveTowards(this.transform.position, coffeeLineWaypoint.transform.position, 1f * Time.deltaTime);
        }
        else
        {
            //Check if there's coffee to be bought
            if(coffeeStandScript.GetNumberOfCoffees() >= 1)
            {
                blackboard.SendEvent("foundCoffee");
            }
        }
	}

    private void FlipCharacter()
    {
        if ((coffeeLineWaypoint.transform.position - this.transform.position).x > 0)
        {
            this.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }

    }
}


