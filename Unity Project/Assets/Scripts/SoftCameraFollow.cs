﻿using UnityEngine;
using System.Collections;

public class SoftCameraFollow : MonoBehaviour {

    public Transform followTarget;
    public float speed;
    
	
	void Update () {
	    if(followTarget != null){
            transform.position = Vector3.Lerp(this.transform.position, new Vector3(followTarget.transform.position.x, this.transform.position.y, followTarget.transform.position.z - 10), speed * Time.deltaTime);
        }
	}
}
