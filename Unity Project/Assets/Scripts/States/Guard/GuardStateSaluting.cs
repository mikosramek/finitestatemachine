﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class GuardStateSaluting : StateBehaviour
{
    private SpriteRenderer rend;
    public Sprite saluting;
    GameObjectVar target;
    GameObjectVar spriteMan;
    //If using a "float"
    //have to use variableName.Value

    void Awake()
    {
        spriteMan = blackboard.GetGameObjectVar("spriteMan");

        rend = spriteMan.Value.GetComponent<SpriteRenderer>();
        target = blackboard.GetGameObjectVar("target");
    }

    // Called when the state is ensabled
    void OnEnable () {
		Debug.Log(gameObject.ToString() + "Started Saluting");
        rend.sprite = saluting;
	}
 
	// Called when the state is disabled
	void OnDisable () {
		Debug.Log(gameObject.ToString() + "Stopped Saluting");
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void FriendlyExitVision(Collider col)
    {
        //Salute
        blackboard.SendEvent("WorkerLeftRange");
        if (target.Value != null)
        {
            blackboard.SendEvent("SeenPlayer");
        }
    }
}


