﻿using UnityEngine;
using System.Collections;

public class BirdVision : MonoBehaviour {

    public string targetTag;
    public GameObject parent;

    void OnTriggerEnter(Collider other)
    {

        if(other.gameObject.tag == targetTag )
        {
            Food tempFood = other.gameObject.GetComponent<Food>();
            if (tempFood.IsActive())
            {
                parent.SendMessage("FoodSpotted", other.gameObject);
            }
        }
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == targetTag)
        {
            Food tempFood = other.gameObject.GetComponent<Food>();
            if (tempFood.IsActive())
            {
                parent.SendMessage("FoodSpotted", other.gameObject);
            }
        }
    }
}
