﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class BirdResting : StateBehaviour
{

    
    public float hungerRate;

    public GameObject[] restingPoints;
    public GameObject restingPoint;
    FloatVar movementSpeed;
    FloatVar hunger;

    public Sprite flying, resting;
    public SpriteRenderer spriteR;
    void Awake()
    {
        hunger = blackboard.GetFloatVar("hunger");
        movementSpeed = blackboard.GetFloatVar("movementSpeed");
        restingPoints = GameObject.FindGameObjectsWithTag("restingPoint");
        restingPoint = restingPoints[Random.Range(0, restingPoints.Length)];

    }

	// Called when the state is enabled
	void OnEnable () {
		//Debug.Log("Started Resting");
        restingPoint = restingPoints[Random.Range(0, restingPoints.Length)];
    }
 
	// Called when the state is disabled
	void OnDisable () {
		//Debug.Log("Stopped Resting");
	}
	
	// Update is called once per frame
	void Update () {
	    if(transform.position != restingPoint.transform.position)
        {
            transform.position = Vector3.MoveTowards(this.transform.position, restingPoint.transform.position, movementSpeed.Value * Time.deltaTime);
            spriteR.sprite = flying;
        }
        else
        {
            spriteR.sprite = resting;
            hunger.Value += hungerRate;
        }

        if(hunger >= 100)
        {
            blackboard.SendEvent("hungry");
        }

	}



}


