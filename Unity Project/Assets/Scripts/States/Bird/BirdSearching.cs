﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class BirdSearching : StateBehaviour
{

    FloatVar movementSpeed;
    FloatVar hunger;
    GameObjectVar foodTarget;
    public Sprite flying;
    public SpriteRenderer spriteR;
    public float startingHeight;
    void Awake()
    {
        hunger = blackboard.GetFloatVar("hunger");
        movementSpeed = blackboard.GetFloatVar("movementSpeed");
        foodTarget = blackboard.GetGameObjectVar("foodTarget");

    }
        // Called when the state is enabled
    void OnEnable () {
		//Debug.Log("Started Searching");
        ChooseFlightPatten();
        spriteR.sprite = flying;
        StartCoroutine(FlyUp());
    }
 
	// Called when the state is disabled
	void OnDisable () {
		//Debug.Log("Stopped Searching");
	}
	
    IEnumerator FlyUp()
    {
        while(transform.position.y <= startingHeight)
        {
            yield return new WaitForSeconds(0.01f);
            transform.position += transform.up * movementSpeed.Value * Time.deltaTime;
        }
        yield return null;
    }

    IEnumerator FlyForward()
    {
        for(int i = 0; i < 25; i++)
        {
            yield return new WaitForSeconds(0.01f);
            transform.position += transform.forward * movementSpeed.Value * Time.deltaTime;
        }
        ChooseFlightPatten();
        yield return null;
    }
    IEnumerator FlyRight()
    {
        float angle = 0;
        while(angle < 25)
        {
            angle++;
            transform.Rotate(new Vector3(0, 1, 0));
            yield return new WaitForSeconds(0.01f);
        }
        ChooseFlightPatten();
        yield return null;
    }
    IEnumerator FlyLeft()
    {
        float angle = 0;
        while (angle < 25)
        {
            angle++;
            transform.Rotate(new Vector3(0, -1, 0));
            yield return new WaitForSeconds(0.01f);
        }
        ChooseFlightPatten();
        yield return null;
    }

    private void ChooseFlightPatten()
    {
        float rand = Random.Range(0, 5);
        if(rand >= 0 && rand < 3)
        {
            StartCoroutine(FlyForward());
        }else if (rand >= 3 && rand < 4)
        {
            StartCoroutine(FlyRight());
        }else if (rand >= 4 && rand <= 5)
        {
            StartCoroutine(FlyLeft());
        }
    }

    public void FoodSpotted(GameObject target)
    {
        foodTarget.Value = target;
        StopAllCoroutines();
        blackboard.SendEvent("foundFood");
    }


}


